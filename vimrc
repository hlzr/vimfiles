execute pathogen#infect()

"----- Basic Behavior and appearance stuff ------------------------------------
set nocompatible
set expandtab
filetype plugin indent on
syntax on

source $VIMRUNTIME/mswin.vim
behave mswin 
set backspace=indent,eol,start

set background=dark
colorscheme desert 

" Colorcolumn for column 80 and 120 onwards:
highlight ColorColumn ctermbg=235 guibg=#2c2d27
let &colorcolumn="80,".join(range(120,999),",")

if has("gui_running")
  " Turn off Title Bar:
  set guioptions-=T  

  " Set the font
  if has("gui_gtk2")
    set guifont=Inconsolata\ 12
  elseif has("gui_macvim")
    set guifont=Monaco:h14
    set noantialias
  elseif has("gui_win32")
    set guifont=Consolas:h11:cANSI
  endif
endif 

"----- More behavior stuff ----------------------------------------------------
"set viminfo='10,\"100,:20,%,n~/_viminfo

let g:DirDiffExcludes = ".git,*.swp,*.un~,*.hex"

" Restore cursor to previous position on file open
function! ResCur()
  if line("'\"") <= line("$")
    normal! g`"
    return 1
  endif
endfunction
augroup resCur
  autocmd!
  autocmd BufWinEnter * call ResCur()
augroup END

" Highlight TODO, FIXME, NOTE, etc.
autocmd Syntax * call matchadd('Todo',  '\W\zs\(TODO\|FIXME\|CHANGED\|XXX\|BUG\|HACK\)')
autocmd Syntax * call matchadd('Debug', '\W\zs\(NOTE\|INFO\|IDEA\)')

" temp, swap, and undo file directories:
set udf
if has('win32') || has('win64')
  set dir=C:\\temp\\vim\\swap//,.,c:\\temp
  set backupdir=C:\\temp\\vim\\backup//,.,c:\\temp
  set udir=C:\\temp\\vim\\undo//,.,c:\\temp
endif

" ctags setup
set tags=tags; " Recursively search for tags

set nowrap

" nerdtree
map <F2> :NERDTreeToggle<CR>
map <F3> :NERDComToggleComment<CR>

" vhdl indent
let g:vhdl_indent_genportmap = 0

"----- Nifty functions --------------------------------------------------------

" CDC = Change to Directory of Current file
command CDC cd %:p:h

" Diffs current buffer with file on disk:
function! s:DiffWithSaved()
  let filetype=&ft
  diffthis
  vnew | r # | normal! 1Gdd
  diffthis
  exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction
com! DiffSaved call s:DiffWithSaved()

" Restore cursor to previous position on file open
function! ResCur()
  if line("'\"") <= line("$")
    normal! g`"
    return 1
  endif
endfunction
augroup resCur
  autocmd!
  autocmd BufWinEnter * call ResCur()
augroup END

" vim: ts=2 sw=2 et
